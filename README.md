# project-timeline

Show project activities in a timeline (a.k.a. gantt) style

## Attributions
<div>Project icon made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
