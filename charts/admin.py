from django.contrib import admin

from .models import DayOff, Project, Task

admin.site.register(DayOff)
admin.site.register(Project)
admin.site.register(Task)
