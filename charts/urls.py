from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:project_id>/timeline', views.view_timeline, name='view_timeline')
]