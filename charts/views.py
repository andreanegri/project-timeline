from django.http import HttpResponse
from django.shortcuts import render
from .logic import create_timeline, days_off, timeline_span
from .models import Project, DayOff

def index(request):
    prj = Project.objects.all()

    context = {
        'projects': prj
    }

    return render(request, 'charts/index.html', context)

def view_timeline(request, project_id):
    project = Project.objects.get(pk=project_id)
    timeline = create_timeline(project)
    daysoff = DayOff.objects.filter(project__id=project_id)

    total = 0

    for entry in timeline:
        total += entry['task_duration']

    context = {
        'project': project,
        'timeline': timeline,
        'range': timeline_span(timeline),
        'days_off': daysoff,
        'total_duration': total
    }

    return render(request, 'charts/timeline.html', context)
