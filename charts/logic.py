from datetime import timedelta
from .models import DayOff, Project, Task
import math


def projects():
    """Return all projects on db"""
    return Project.objects.values_list('name', 'id')

def project_tasks(id):
    """Return tasks related to project"""
    return Task.objects.filter(project__id=id)

def days_off(id):
    """Days off related to project"""
    return DayOff.objects.filter(project__id=id).values_list('date', flat=True)

def create_timeline(prj):
    tasks = project_tasks(prj.id)
    prj_start_date = prj.start_date
    holidays = days_off(prj.id)

    timeline = []

    current_task_start = prj_start_date
    
    for i in range(len(tasks)):
        entry = {}
        entry['task_id'] = tasks[i].id
        entry['start_date'] = current_task_start
        entry['task_name'] = tasks[i].name
        entry['task_tag'] = tasks[i].tag
        entry['task_duration'] = math.ceil(tasks[i].duration * prj.effort)
        
        current_task_end = activity_deadline(current_task_start, entry['task_duration'], holidays)
        entry['end_date'] = current_task_end

        if i==0:
            entry['dependency'] = None
        else:
            entry['dependency'] = tasks[i-1].id

        timeline.append(entry)

        current_task_start = next_workday(current_task_end, holidays)
    
    return timeline

def timeline_span(timeline):
    return (timeline[0]['start_date'], timeline[-1]['end_date'])

def is_workday(work_date, days_off, weekends=(5,6)):
    return work_date not in days_off and work_date.weekday() not in weekends

def next_workday(current_work_date, days_off, weekends=(5,6)):
    while True:
        current_work_date += timedelta(days=1)
        if is_workday(current_work_date, days_off, weekends):
            return current_work_date

def activity_deadline(start_date, duration, holidays=[], weekends=(5,6)):
    calculated_duration = 0
    current_date = start_date

    while calculated_duration < duration:
        if is_workday(current_date, holidays, weekends):
            calculated_duration += 1
            if calculated_duration == duration:
                return current_date
        current_date += timedelta(days=1)

    return current_date