from django.db import models

class DayOff(models.Model):
    date = models.DateField()
    description = models.CharField(max_length=200, blank=True)

    class Meta:
        ordering = ('date',)

    def __str__(self):
        return '{} ({})'.format(self.date, self.description)

class Project(models.Model):
    effort_dict = [
        (1.0, 'fulltime'),
        (1.2, '80%'),
        (1.4, '60%'),
        (1.6, '40%'),
        (1.8, '20%')
    ]

    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    start_date = models.DateField('start date')
    days_off = models.ManyToManyField(DayOff, blank=True)
    effort = models.FloatField(default=1.0, choices=effort_dict)

    @property
    def effort_str(self):
        return self.get_effort_display()

    def __str__(self):
        return self.name

class Task(models.Model):
    name = models.CharField(max_length=200)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    priority = models.IntegerField()
    duration = models.IntegerField('duration (days)')
    tag = models.CharField(max_length=200, blank=True)

    class Meta:
        ordering = ('project', 'priority', 'name',)

    def __str__(self):
        return '[{}] {} ({} days)'.format(self.project, self.name, self.duration)

