from django.test import TestCase
from .logic import projects, project_tasks,days_off, is_workday, next_workday, activity_deadline, create_timeline
from datetime import date
from .models import Project, Task, DayOff

class LogicTestCase(TestCase):
    def setUp(self):
        d1 = DayOff.objects.create(date=date(year=2021, month=12, day=8))
        d2 = DayOff.objects.create(date=date(year=2021, month=12, day=25))

        prj_1 = Project.objects.create(name='Test project', start_date=date(year=2021, month=12, day=1))
        prj_1.days_off.set([d1, d2])
        prj_1.save()

        Task.objects.create(name='Task #1', project=prj_1, priority=1, duration=1, tag='dev')
        Task.objects.create(name='Task #2', project=prj_1, priority=2, duration=3, tag='dev')
    
    def test_return_all_projects(self):
        prjs = projects()
        db_prjs = Project.objects.all()
        self.assertEqual(len(prjs), len(db_prjs))
        self.assertEqual(prjs[0][0], db_prjs[0].name)
        self.assertEqual(prjs[0][1], db_prjs[0].id)

    def test_return_project_tasks(self):
        prj_id=1
        tasks = project_tasks(prj_id)
        db_tasks = Task.objects.all()
        self.assertEqual(len(tasks), len(db_tasks))
    
    def test_return_days_off(self):
        prj_id=1
        db_prjs = DayOff.objects.filter(project__id=prj_id)
        daysoff = days_off(prj_id)
        self.assertEqual(len(daysoff), len(db_prjs))

    def test_work_day(self):
        # 01/12/2021 is a work day (Wednesday)
        self.assertTrue(is_workday(date(year=2021, month=12, day=1), days_off(1)))

        # 08/12/2021 is not a work day (added in days off)
        self.assertFalse(is_workday(date(year=2021, month=12, day=8), days_off(1)))

        # 04/12/2021 is not a work day (Saturday)
        self.assertFalse(is_workday(date(year=2021, month=12, day=4), days_off(1)))

    def test_next_work_day(self):
        # next workday is Monday if current day is Friday
        current_date = date(year=2021, month=12, day=3)
        self.assertEqual(next_workday(current_date, days_off(1)), date(year=2021, month=12, day=6))

        # next workday is Dec. 9 if Dec. 8 is day-off
        current_date = date(year=2021, month=12, day=7)
        self.assertEqual(next_workday(current_date, days_off(1)), date(year=2021, month=12, day=9))

        current_date = date(year=2021, month=12, day=5)
        self.assertEqual(next_workday(current_date, days_off(1)), date(year=2021, month=12, day=6))

    def test_activity_deadline(self):
        start_date = date(year=2021, month=12, day=1)
        self.assertEqual(activity_deadline(start_date, duration=1, holidays=days_off(1)), date(year=2021, month=12, day=1))
        self.assertEqual(activity_deadline(start_date, duration=2, holidays=days_off(1)), date(year=2021, month=12, day=2))
        self.assertEqual(activity_deadline(start_date, duration=3, holidays=days_off(1)), date(year=2021, month=12, day=3))
        self.assertEqual(activity_deadline(start_date, duration=4, holidays=days_off(1)), date(year=2021, month=12, day=6))
        self.assertEqual(activity_deadline(start_date, duration=5, holidays=days_off(1)), date(year=2021, month=12, day=7))
        self.assertEqual(activity_deadline(start_date, duration=6, holidays=days_off(1)), date(year=2021, month=12, day=9))

    def test_create_timeline(self):
        prj = Project.objects.get(pk=1)
        timeline = create_timeline(prj)

        self.assertEqual(len(timeline), 2)
        self.assertEqual(timeline[0]['start_date'], prj.start_date)
        self.assertEqual(timeline[0]['end_date'],   date(year=2021, month=12, day=1))
        self.assertEqual(timeline[1]['start_date'], date(year=2021, month=12, day=2))
        self.assertEqual(timeline[1]['end_date'],   date(year=2021, month=12, day=6))